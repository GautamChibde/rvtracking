package com.example.gautam.rvtracking.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.gautam.rvtracking.App
import com.example.gautam.rvtracking.R
import com.example.gautam.rvtracking.main.adapter.ItemAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info

class MainActivity : AppCompatActivity(), MainActivityView, AnkoLogger {
    private val presenter: MainActivityPresenter by lazy { MainActivityPresenter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        presenter.init()
        setData()
    }

    private fun initView() {
        main_activity_rv_items.layoutManager = LinearLayoutManager(this)
        main_activity_rv_items.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                when (newState) {
                    RecyclerView.SCROLL_STATE_IDLE -> {
                        presenter.startTracking(Pair(
                                (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition(),
                                (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()))
                    }
                }
            }
        })
        main_activity_rv_items.viewTreeObserver.addOnGlobalLayoutListener({
            presenter.startTracking(Pair(
                    (main_activity_rv_items.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition(),
                    (main_activity_rv_items.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()))
        })
    }

    private fun setData() {
        main_activity_rv_items.adapter = ItemAdapter((application as App).getDummyData())
    }

    override fun trackingItems(trackedItem: Int) {
        info(String.format(getString(R.string.tracking_item), (trackedItem + 1)))
    }

    override fun onError(message: String?) {
        error { "error $message" }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }
}
