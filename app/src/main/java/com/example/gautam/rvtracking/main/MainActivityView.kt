package com.example.gautam.rvtracking.main

/**
 * Created by gautam on 10/7/18.
 */
interface MainActivityView {
    fun trackingItems(trackedItem: Int)
    fun onError(message: String?)
}