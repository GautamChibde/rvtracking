package com.example.gautam.rvtracking

import android.app.Application

/**
 * Created by gautam on 9/7/18.
 */

class App : Application() {

    private lateinit var dummyData : ArrayList<String>

    override fun onCreate() {
        super.onCreate()
        dummyData = ArrayList()
        (1..25).forEach {
            dummyData.add("item $it")
        }
    }

    fun getDummyData() : ArrayList<String> = dummyData
}
