package com.example.gautam.rvtracking.main

import com.example.gautam.rvtracking.main.interactor.FilterTrackedItemInteractor
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by gautam on 10/7/18.
 */
class MainActivityPresenter(private val view: MainActivityView) {
    val publishSubject: PublishSubject<Pair<Int, Int>> = PublishSubject.create()
    private val disposable = CompositeDisposable()
    private var oldRange = Pair(-1, -1)

    fun init() {
        publishSubject.distinctUntilChanged()
                .throttleWithTimeout(DELAY_TIME_MILLISECONDS, TimeUnit.MILLISECONDS)
                .flatMap { Observable.fromIterable(it.first..it.second) }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ view.trackingItems(it) },
                        { error -> view.onError(error.message) })
                .addTo(disposable)
    }

    fun startTracking(newRange: Pair<Int, Int>) {
        if (oldRange != newRange) {
            FilterTrackedItemInteractor().execute(oldRange, newRange)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.single())
                    .subscribe({
                        it?.let { publishSubject.onNext(it) }
                    }, { error -> view.onError(error.message) })
            oldRange = newRange
        }
    }

    fun dispose() = disposable.dispose()

    companion object {
        const val DELAY_TIME_MILLISECONDS: Long = 300
    }
}