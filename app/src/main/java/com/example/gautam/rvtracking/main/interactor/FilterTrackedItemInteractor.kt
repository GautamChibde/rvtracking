package com.example.gautam.rvtracking.main.interactor

import io.reactivex.Observable
import io.reactivex.Single

class FilterTrackedItemInteractor {
    fun execute(oldRange: Pair<Int, Int>, newRange: Pair<Int, Int>) : Single<Pair<Int, Int>> {
        val existingItems = (oldRange.first..oldRange.second)
        return Observable.fromIterable(newRange.first..newRange.second)
                .filter { !existingItems.contains(it) }
                .sorted()
                .toList()
                .map { t -> if (t.isNotEmpty()) Pair(t.first(), t.last()) else null }
    }
}