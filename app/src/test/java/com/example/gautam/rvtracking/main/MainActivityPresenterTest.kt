package com.example.gautam.rvtracking.main

import com.example.gautam.rvtracking.main.interactor.FilterTrackedItemInteractor
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

/**
 * Created by gautam on 10/7/18.
 */
class MainActivityPresenterTest {
    private lateinit var presenter: MainActivityPresenter
    @Mock
    private lateinit var view: MainActivityView

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MainActivityPresenter(view)
    }

    @Test
    fun _test_if_items_are_successfully_emitted_after_300ms_() {
        // mock data
        val testRange = Pair(1, 6)

        val scheduler = TestScheduler()

        val ts = presenter.publishSubject
                .throttleWithTimeout(MainActivityPresenter.DELAY_TIME_MILLISECONDS, TimeUnit.MILLISECONDS, scheduler)
                .flatMap { Observable.fromIterable(it.first..it.second) }
                .test()

        ts.assertEmpty()

        presenter.publishSubject.onNext(testRange)

        scheduler.advanceTimeBy(299, TimeUnit.MILLISECONDS)

        // test whether no items are emitted before 300ms
        ts.assertEmpty()

        scheduler.advanceTimeBy(1, TimeUnit.MILLISECONDS)

        // test if item successfully emitted after 300ms
        ts.assertNoErrors()
        assertEquals(arrayListOf(1, 2, 3, 4, 5, 6), ts.values())
    }


    @Test
    fun _test_if_items_are_interrupted_before_300ms_() {
        // mock data
        val testRange1 = Pair(1, 6)
        val testRange2 = Pair(2, 7)

        val scheduler = TestScheduler()

        val ts = presenter.publishSubject
                .distinctUntilChanged()
                .throttleWithTimeout(300, TimeUnit.MILLISECONDS, scheduler)
                .flatMap { Observable.fromIterable(it.first..it.second) }
                .test()

        ts.assertEmpty()

        presenter.publishSubject.onNext(testRange1)

        scheduler.advanceTimeBy(200, TimeUnit.MILLISECONDS)

        // test whether no items are emitted before 300ms
        ts.assertEmpty()
        presenter.publishSubject.onNext(testRange2)

        scheduler.advanceTimeBy(299, TimeUnit.MILLISECONDS)

        // test if no item are emitted after interruption
        ts.assertEmpty()

        scheduler.advanceTimeBy(1, TimeUnit.MILLISECONDS)

        // test if interruption item successfully emitted after 300ms
        ts.assertNoErrors()
        assertEquals(arrayListOf(2, 3, 4, 5, 6, 7), ts.values())
    }

    @Test
    fun _test_correct_output_for_filterTrackedItems_() {
        // test data
        val testOldState = Pair(1, 5)
        val testNewState = Pair(4, 8)
        val expectedResult = Pair(6, 8)

        // Test Correct result
        FilterTrackedItemInteractor().execute(testOldState, testNewState)
                .test()
                .assertValue(expectedResult)

        // test data
        val testOldState2 = Pair(20, 26)
        val testNewState2 = Pair(16, 22)
        val expectedResult2 = Pair(16, 19)

        // Test Correct result
        FilterTrackedItemInteractor().execute(testOldState2, testNewState2)
                .test()
                .assertValue(expectedResult2)
    }

    @Test
    fun _test_null_output_for_filterTrackedItems_() {
        // test data
        val testOldState = Pair(1, 5)
        val testNewState = Pair(2, 5)

        //  Test for null output
        FilterTrackedItemInteractor().execute(testOldState, testNewState)
                .test()
                .assertNoValues()

        // test data
        val testOldState1 = Pair(1, 5)
        val testNewState1 = Pair(-2, -5)

        //  Test for null output
        FilterTrackedItemInteractor().execute(testOldState1, testNewState1)
                .test()
                .assertNoValues()
    }
}